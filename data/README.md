## Dog Breed Data

This data explores the different characteristics of various dog breeds.

To run this file, please follow the bellow steps:
1. Navigate to a folder where you would like to clone this repository
2. Once inside, please copy and paste the below line into the terminal
```
git clone git@gitlab.oit.duke.edu:sf282/week1-code.git
```

3. Once you have cloned the repo, please go to "Week1 - Code+" folder
4. Then click on the "data" folder
5. Open the file "susan_demo1.ipynb"
6. Once the file is open, run through file and the code chunks 